﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemy;
    public GameObject elite;
    private float eliteChance = 20f / 100f;

    void Start()
    {
        StartCoroutine(spawn());
        
    }
    IEnumerator spawn()
    {
        
        while (GameManager.Instance.enemies2spawn > 0)
        {

            yield return new WaitForSeconds(Random.Range(1f, 2f));
            if (GameManager.Instance.enemies2spawn > 0)
            {
                float r = Random.Range(0f, 1f);
                if (r < eliteChance)
                {

                    Instantiate(elite, new Vector3(this.transform.position.x, this.transform.position.y, 0), new Quaternion(0, 0, 0, 0));
                    GameManager.Instance.enemies2spawn--;
                }
                else
                {
                    Instantiate(enemy, new Vector3(this.transform.position.x, this.transform.position.y, 0), new Quaternion(0, 0, 0, 0));
                    GameManager.Instance.enemies2spawn--;
                }
              
            }
        }
    }
}
