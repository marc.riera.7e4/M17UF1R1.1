﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GridSpwan : MonoBehaviour
{
    GameObject[] grids;

    [SerializeField]
    public string carpeta;


    void Start()
    {
        grids = Resources.LoadAll(carpeta, typeof(GameObject)).Cast<GameObject>().ToArray();
        GameObject obj = Instantiate(grids[Random.Range(0, grids.Length)], transform.position, Quaternion.identity);

        obj.transform.parent = this.transform;
    }
}