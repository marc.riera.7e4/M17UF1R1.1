﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class weapomSpawn : MonoBehaviour
{
    [SerializeField]
    public string carpeta;

    WeaponSc thisWeapon;
    WeaponSc[] weapons;
    void Start()
    {

        Debug.Log("creat");
        weapons = Resources.LoadAll<WeaponSc>(carpeta);
        thisWeapon = weapons[Random.Range(0, weapons.Length)];
        Debug.Log(weapons[0]);
        gameObject.GetComponent<SpriteRenderer>().sprite = thisWeapon.icon;
        gameObject.AddComponent<BoxCollider2D>();
        gameObject.GetComponent<BoxCollider2D>().isTrigger = true;

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player")){
            GameManager.Instance.getWeapon(thisWeapon);
            Destroy(gameObject);
        }
    }

}
