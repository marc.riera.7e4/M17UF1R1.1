﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prop : MonoBehaviour
{
    bool destroyed = false;
    public GameObject weapon;

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Bullet"))
        {
            //Debug.Log("crear");
            GameObject WeaponPickUp = Instantiate(weapon, transform.position, Quaternion.identity);
            Destroy(gameObject);
            
        }
    }
}
