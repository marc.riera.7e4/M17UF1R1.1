﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropSpawner : MonoBehaviour
{
    public GameObject[] props;
    public int[] positionsX;
    public int[] positionsY;

    public int minX;
    public int maxX;
    public int minY;
    public int maxY;
    private bool repeated = false;
    void Start()
    {
        for (int i = 0; i < 50; i++)
        {
            repeated = false;
            int randomX = Random.Range(minX, maxX);
            positionsX[i] = randomX;
            int randomY = Random.Range(minY, maxY);
            positionsX[i] = randomY;
            for (int z = 0; z < i; z++)
            {
                if(randomX == positionsX[z] && randomY == positionsY[z])
                {
                    repeated = true;
                }
            }
            if (repeated == false) {
                int prop = Random.Range(0, props.Length);
                Instantiate(props[prop], new Vector3(randomX, randomY), Quaternion.identity);
            } else
            {
                i--;
            }
        }
    }

  
}
