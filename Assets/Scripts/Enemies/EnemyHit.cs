﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHit : MonoBehaviour
{
    public extraEffect extraEffect;
    public float hp;
    public int dmg;
    private int original;
    private void Start()
    {
        hp = hp * GameManager.Instance.enemyHpMult;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Bullet"))
        {
            GameManager.Instance.shotsHit++;
            GameManager.Instance.morePunt(5);
            hp = hp - collision.gameObject.GetComponent<BulletBehaviour>().weapon.dmg;
            if(collision.gameObject.GetComponent<BulletBehaviour>().weapon.extraEffect == extraEffect.freeze)
            {
                frezee(4);
            }
            if (hp <= 0)
            {
                GameManager.Instance.zombieSound(gameObject.GetComponent<AudioSource>().clip);
                GameManager.Instance.morePunt(15);
                GameManager.Instance.enemiesKilled++;
                Destroy(gameObject);
                
            }
           
        }
        if (collision.gameObject.CompareTag("Player"))
        {
            GameManager.Instance.Damage(dmg);
            
            
        }
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            GameManager.Instance.shotsHit++;
            GameManager.Instance.morePunt(5);
            hp = hp -collision.gameObject.GetComponent<BulletBehaviour>().weapon.dmg;
            if (hp <= 0)
            {
                GameManager.Instance.zombieSound(gameObject.GetComponent<AudioSource>().clip);
                GameManager.Instance.enemiesKilled++;
                GameManager.Instance.morePunt(15);
                Destroy(gameObject);
                
            }
        }
        if (collision.gameObject.CompareTag("Player"))
        {
            GameManager.Instance.Damage(dmg);

        }
    }
    public void frezee(float time)
    {
        gameObject.GetComponent<SpriteRenderer>().color = Color.blue;
        original = gameObject.GetComponent<EnemyMovement>().speed;
        gameObject.GetComponent<EnemyMovement>().speed = 0;
        StartCoroutine(timer(time, original));
    }
    public IEnumerator timer(float time, int original) 
    {
        yield return new WaitForSeconds(time);
        gameObject.GetComponent<SpriteRenderer>().color = Color.white;
        gameObject.GetComponent<EnemyMovement>().speed = original;
    }
}

