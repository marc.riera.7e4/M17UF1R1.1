﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dontDestroyOnLoad : MonoBehaviour
{
    public static dontDestroyOnLoad _instance;
    public static dontDestroyOnLoad Instance { get { return _instance; } }
    private void Awake()
    {
        if (_instance == null)
        {
            DontDestroyOnLoad(gameObject);
            _instance = this;

        }
        else
        {
            Destroy(gameObject);
        }
    }
}
