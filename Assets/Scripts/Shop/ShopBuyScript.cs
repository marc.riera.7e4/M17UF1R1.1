﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopBuyScript : MonoBehaviour
{

    [SerializeField]
    ShopItem[] shopItem;
    public Text costText;
    bool claimed;
    public int numItem;
    public int shopSlot;

    void Start()
    {
        
        costText.text = shopItem[GameManager.Instance.shopBought[shopSlot]].price.ToString();
    }

    private void Update()
    {
        if (shopItem.Length <= GameManager.Instance.shopBought[shopSlot])
        {
            gameObject.GetComponent<Button>().interactable = false;
            costText.text = "SOLD";
        }
        else
        {
            costText.text = shopItem[GameManager.Instance.shopBought[shopSlot]].price.ToString();
        }
        
    }

    public void Buy()
    {
        if (GameManager.Instance.money >= shopItem[numItem].price)
        {
            GameManager.Instance.shopBought[shopSlot]++;
            if (shopItem[numItem].stat == Stat.Hp)
            {
                if (shopItem[numItem].character == Char.Player)
                {
                    GameManager.Instance.maxhp = GameManager.Instance.maxhp * shopItem[0].mult;
                    GameManager.Instance.hp = GameManager.Instance.maxhp;
                    GameManager.Instance.money = GameManager.Instance.money - shopItem[numItem].price;
                    
                }
                else if (shopItem[numItem].character == Char.Enemy)
                {
                    GameManager.Instance.enemyHpMult *= shopItem[numItem].mult;
                    GameManager.Instance.money = GameManager.Instance.money - shopItem[numItem].price;
                    
                }
            }
            else if (shopItem[numItem].stat == Stat.Speed)
            {
                GameManager.Instance.playerSpeed += Mathf.RoundToInt(shopItem[numItem].mult);
                GameManager.Instance.money = GameManager.Instance.money - shopItem[numItem].price;
                
            }
            else if (shopItem[numItem].stat == Stat.Damage)
            {
                GameManager.Instance.dmgMult *= shopItem[numItem].mult;
                GameManager.Instance.money = GameManager.Instance.money - shopItem[numItem].price;
                
            }
            else if (shopItem[numItem].stat == Stat.FireRate)
            {
                GameManager.Instance.RateMult *= shopItem[numItem].mult;
                GameManager.Instance.money = GameManager.Instance.money - shopItem[numItem].price;
                
            }
            else if (shopItem[numItem].stat == Stat.Ammo)
            {
                GameManager.Instance.AmmoMult *= shopItem[numItem].mult;
                GameManager.Instance.money = GameManager.Instance.money - shopItem[numItem].price;
                
            }
        }
    }
}