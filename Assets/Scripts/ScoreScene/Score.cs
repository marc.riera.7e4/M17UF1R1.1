﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public enum Scores { Shots, Enemies, Hits, HighScore }
    public Scores actualScore;
    void Start()
    {
        switch (actualScore)
        {
            case Scores.Shots:
                gameObject.GetComponent<Text>().text = GameManager.Instance.shotsHit+"";
                break;
            case Scores.Enemies:
                gameObject.GetComponent<Text>().text = GameManager.Instance.enemiesKilled + "";
                break;
            case Scores.Hits:
                gameObject.GetComponent<Text>().text = GameManager.Instance.hits2Player + "";
                break;
            case Scores.HighScore:
                int actualScore = GameManager.Instance.score;
                int high = GameManager.Instance.highScore;

                if (actualScore > high)
                {
                    gameObject.GetComponent<Text>().text = GameManager.Instance.score + "";
                    GameManager.Instance.highScore = GameManager.Instance.score;
                }
                else
                {
                    gameObject.GetComponent<Text>().text = GameManager.Instance.highScore+"";
                }
                break;
        }
    }

}
