﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetLevel21 : MonoBehaviour
{
    // Start is called before the first frame update
    public void nextLevel()
    {
        if (GameManager.Instance.nextLevel == false)
        {
            GameManager.Instance.level = 1;
            GameManager.Instance.score = 0;
            GameManager.Instance.enemiesKilled = 0;
            GameManager.Instance.hits2Player = 0;
            GameManager.Instance.shotsHit = 0;
            GameManager.Instance.enemies2spawn = 10;
        }

    }
}
