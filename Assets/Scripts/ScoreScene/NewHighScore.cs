﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewHighScore : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        int actualScore = GameManager.Instance.score;
        int high = GameManager.Instance.highScore;
        if (actualScore > high)
        {
            gameObject.GetComponent<Text>().text = "NEW HIGH SCORE";
        }
        else
        {
           
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
