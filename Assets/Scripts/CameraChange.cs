﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraChange : MonoBehaviour
{
    public int minX;
    public int maxX;
    public int minY;
    public int maxY;
    // Start is called before the first frame update
    void Start()
    {
        GameObject.Find("Main Camera").GetComponent<CameraFollow>().minX = minX;
        GameObject.Find("Main Camera").GetComponent<CameraFollow>().maxX = maxX;
        GameObject.Find("Main Camera").GetComponent<CameraFollow>().minY = minY;
        GameObject.Find("Main Camera").GetComponent<CameraFollow>().maxY = maxY;
    }

    
}
