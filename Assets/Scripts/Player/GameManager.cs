﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public Animator animatorPlayer;
    public int playerSpeed = 5;
    public static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }
    public bool move = false;
    public Vector2 idleLeft;
    public bool flipWeapon=false;
    public float maxhp = 100;
    public float hp = 100;
    public  int enemies2spawn = 20;
    public int weapon = 0;
    private GameObject[] enemies;
    public int score;
    public int money;

    public AudioClip deathSound;

    public int highScore;
    public int shotsHit;
    public int hits2Player;
    public int enemiesKilled;

    public float enemyHpMult = 1;

    public float dmgMult = 0;
    public float RateMult = 0;
    public float AmmoMult = 0;

    public int[] shopBought;

    public string lastScene;

    public bool invin;
    public int level = 1;
    public bool nextLevel = false;
    [SerializeField]
    public List<WeaponSc> Weapons;

    private void Awake()
    {
        if (_instance == null)
        {
            DontDestroyOnLoad(gameObject);
            _instance = this;

        }
        else
        {
            Destroy(gameObject);
        }
    }
    public void Damage(int dmg)
    {
        if (invin == false)
        {
            score -= dmg;
            hits2Player--;
            hp = hp - dmg;
            invincible(0.5f);
            
        }
    }

    public void invincible(float time)
    {
        invin = true;
        StartCoroutine(invinTimer(time));
    }

    public IEnumerator invinTimer(float time)
    {
        
        yield return new WaitForSeconds(time);
        invin = false;

    }
    private void Start()
    {
        try
        {
           highScore = PlayerPrefs.GetInt("HighScore");
        }
        catch (System.Exception error)
        {
            Debug.Log($"SaveData: {error}");
        }
    }
    private void Update()
    {
        Debug.Log(Weapons.Count);
    enemies = GameObject.FindGameObjectsWithTag("Enemy");
        animatorPlayer = GameObject.Find("Player").GetComponent<Animator>();
        if (move == true)
        {
            animatorPlayer.SetBool("Move", true);
        } else
        {
            animatorPlayer.SetBool("Move", false);
        }
        if (hp <= 0)
        {
            Debug.Log("restart");
            gameObject.GetComponent<AudioSource>().Play();
            hp = 100;
            enemies2spawn = 20;
            Weapons.Clear();
            weapon = 0;
            nextLevel = false;
            level = 1;
            for (int i = 0; i < shopBought.Length; i++)
            {
                shopBought[i] = 0;
            }
            dmgMult = 1;
            AmmoMult = 1;
            RateMult = 1;
            enemyHpMult = 1;
            maxhp = 100;
            hp = 100;
            playerSpeed = 5;
            try
            {
                PlayerPrefs.SetInt("HighScore", highScore);
            }
            catch (System.Exception error)
            {
                Debug.Log($"SaveData: {error}");
            }

            SceneManager.LoadScene("Score");
        }
        if (enemies.Length == 0 && enemies2spawn == 0)
        {
            Debug.Log("restart");
            Weapons.Clear();
            weapon = 0;
            hp = maxhp;
            nextLevel = true;
            level++;
            enemies2spawn = level*10;
            money += score;

            try
            {
                PlayerPrefs.SetInt("HighScore", highScore);
            }
            catch (System.Exception error)
            {
                Debug.Log($"SaveData: {error}");
            }

            SceneManager.LoadScene("Score");

        }
        
    }
    public void nextWeapon()
    {
        weapon++;
        if (weapon > Weapons.Count-1)
        {
            weapon = 0;
        }
    }
    public void previousWeapon()
    {
        weapon--;
        if (weapon < 0)
        {
            weapon = Weapons.Count-1;
        }
    }
    public void getWeapon(WeaponSc weapon)
    {
        bool repe = false;
        for(int i = 0; i < Weapons.Count; i++)
        {
            if(Weapons[i].ID == weapon.ID)
            {
                Weapons[i].ammo = Weapons[i].ammo + Weapons[i].maxBulletCapacity / 2;
                repe = true;    
                break;
            }
        }
        if (repe == false)
        {
            WeaponSc copy = Instantiate(weapon);
            copy.dmg *= dmgMult;
            copy.fireRate *= RateMult;
            copy.ammo *= AmmoMult;
            Weapons.Add(copy);
        }
    }
    public void morePunt(int points) {
        score += points;    
    }

    public void zombieSound(AudioClip audio)
    {
        gameObject.GetComponent<AudioSource>().PlayOneShot(audio);
    }
}