﻿using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    public Transform target;

    public float smoothSpeed = 0.125f;
    public Vector3 offset;

    public int minX;
    public int maxX;
    public int minY;
    public int maxY;

    void Update()
    {
        Vector3 desiredPosition = target.position + offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
        transform.position = new Vector3(Mathf.Clamp(smoothedPosition.x, minX, maxX),Mathf.Clamp(smoothedPosition.y, minY, maxY),-10);
    }

}