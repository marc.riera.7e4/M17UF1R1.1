﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookMouseGun : MonoBehaviour
{
    private Vector3 mouse_pos;
    private Transform target; //Assign to the object you want to rotate
    private Vector3 object_pos;
    private float angle;
    private bool shoot = true;
    private float scale2change;

    
    public GameObject bullet;
    

    public WeaponSc initialweapon;
    WeaponSc weapon;



    private void Start()
    {
        scale2change = transform.parent.localScale.y;
        WeaponSc copy = Instantiate(initialweapon);
        GameManager.Instance.Weapons.Add(copy);
    }
    void Update()
    {
        weapon = GameManager.Instance.Weapons[GameManager.Instance.weapon];
        gameObject.GetComponent<SpriteRenderer>().sprite = weapon.icon;
        mouse_pos = Input.mousePosition;
        mouse_pos.z = 5.23f; //The distance between the camera and object
        object_pos = Camera.main.WorldToScreenPoint(transform.position);
        mouse_pos.x = mouse_pos.x - object_pos.x;
        mouse_pos.y = mouse_pos.y - object_pos.y;
        angle = Mathf.Atan2(mouse_pos.y, mouse_pos.x) * Mathf.Rad2Deg;
        if (transform.localEulerAngles.z > 90 && transform.localEulerAngles.z < 270)
        {
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
            gameObject.GetComponent<SpriteRenderer>().flipY = true;
        }
        else
        {
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
            gameObject.GetComponent<SpriteRenderer>().flipY = false;
        }

        if (GameManager.Instance.flipWeapon == true)
        {
            gameObject.GetComponent<SpriteRenderer>().flipX = true;
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().flipX = false;
        }

        // Esconder arma al moverse
        if (GameManager.Instance.move == true)
        {
            gameObject.GetComponent<SpriteRenderer>().enabled = false;

        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().enabled = true;
        }

        if (Input.GetMouseButtonDown(0) && shoot == true && GameManager.Instance.move == false && weapon.ammo>0)
        {
            gameObject.GetComponent<AudioSource>().PlayOneShot(weapon.sound);
            for (int i = 0; i < weapon.bulletXShoot; i++)
            {

                GameObject bulletClone = Instantiate(bullet);
                bulletClone.transform.position = this.transform.position;
                
                    bulletClone.transform.rotation = Quaternion.Euler(0, 0, angle);
                if (i == 0)
                {
                    bulletClone.GetComponent<Rigidbody2D>().velocity = transform.right * weapon.speed;
                }
                else
                {
                    bulletClone.GetComponent<Rigidbody2D>().velocity = new Vector3(transform.right.x, Random.Range(transform.right.y-0.7f, transform.right.y+ 0.7f), 0) * weapon.speed;
                }
                
                bulletClone.GetComponent<BulletBehaviour>().weapon = weapon;
 
            }
                shoot = false;
            weapon.ammo--;
            StartCoroutine(cd());
        }


        if (Input.GetKeyDown(KeyCode.X))
        {
            Debug.Log("last " + GameManager.Instance.weapon);
                GameManager.Instance.previousWeapon();
            Debug.Log("now " + GameManager.Instance.weapon);

        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            Debug.Log("last "+ GameManager.Instance.weapon);
                GameManager.Instance.nextWeapon();
            Debug.Log("now " + GameManager.Instance.weapon);

        }
    }
    IEnumerator cd()
    {
        
        yield return new WaitForSeconds(weapon.fireRate);
        shoot = true;
    }
    
}