﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{
    public WeaponSc weapon;

    public Vector3 initial;
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player") || col.gameObject.CompareTag("PickUp")|| col.gameObject.CompareTag("Bullet"))
        {

        }
        else
        {
            Debug.Log("destroyed by " + col);
            Destroy(gameObject);
            
        }
    }


    private void Start()
    {
        initial = gameObject.transform.position;

    }

    private void Update()
    {
        if(Vector3.Distance(initial, transform.position) > weapon.range)
        {
            Debug.Log("destroyed by dis");
            Destroy(gameObject);
        }
        if (weapon.extraEffect == extraEffect.freeze)
        {
            gameObject.GetComponent<SpriteRenderer>().color = Color.blue;
        }
    }
}
