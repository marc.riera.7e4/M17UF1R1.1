﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Slider healthBar;
    void Start()
    {
        healthBar = GetComponent<Slider>();
        healthBar.maxValue = 100;
    }

    // Update is called once per frame
    void Update()
    {
        healthBar.value = GameManager.Instance.hp;
    }
}
