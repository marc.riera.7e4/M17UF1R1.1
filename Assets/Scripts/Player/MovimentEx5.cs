﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimentEx5 : MonoBehaviour
{
    SpriteRenderer Sp;
    private bool right;
    Rigidbody2D rb2D;

    private void Start()
    {
        Sp = gameObject.GetComponent<SpriteRenderer>();
        rb2D = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        GameManager.Instance.move = false;
        //horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;
        if (Input.GetKey(KeyCode.A))
        {
            transform.position += Vector3.left * GameManager.Instance.playerSpeed * Time.deltaTime;
            GameManager.Instance.move = true;
            
            
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.position += Vector3.right * GameManager.Instance.playerSpeed * Time.deltaTime;
            GameManager.Instance.move = true;
        }
        if (Input.GetKey(KeyCode.W))
        {
            transform.position += Vector3.up * GameManager.Instance.playerSpeed * Time.deltaTime;
            GameManager.Instance.move = true;
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.position += Vector3.down * GameManager.Instance.playerSpeed * Time.deltaTime;
            GameManager.Instance.move = true;
        }
    }
}


