﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dash : MonoBehaviour
{
    public bool cd = false;
    public float cdTime;

    private Vector3 mousePos;
    private Camera cam;
    public float dashDist = 1;

    private void Start()
    {
        cam = Camera.main;
    }

    void Update()
    {
        mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        var direction = (mousePos - this.transform.position);
        direction.z = 0;
        
        if (cd == false && Input.GetKey(KeyCode.LeftShift))
        {
            Vector3 beforeDash = transform.position;
            Debug.Log("dash");
            direction.Normalize();
            transform.position = transform.position + direction * dashDist;

            GameManager.Instance.invincible(0.5f);

            cd = true;
            StartCoroutine(cdTimer());
        }
    }
    IEnumerator cdTimer()
    {
        yield return new WaitForSeconds(cdTime);
        cd = false;
    }
}
