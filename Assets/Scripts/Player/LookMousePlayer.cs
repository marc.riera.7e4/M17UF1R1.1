﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookMousePlayer : MonoBehaviour
{
    private Vector3 mouse_pos;
    private Transform target; //Assign to the object you want to rotate
    private Vector3 object_pos;
    private float angle;
    public Animator animator;
    private float scale2change;


    void Start()
    {
        scale2change = transform.localScale.x;

    }

    void Update()
    {
        mouse_pos = Input.mousePosition;
        mouse_pos.z = 5.23f; //The distance between the camera and object
        object_pos = Camera.main.WorldToScreenPoint(transform.position);
        mouse_pos.x = mouse_pos.x - object_pos.x;
        mouse_pos.y = mouse_pos.y - object_pos.y;
        angle = Mathf.Atan2(mouse_pos.y, mouse_pos.x) * Mathf.Rad2Deg;
        


        if (angle >= -45 && angle < 45)
        {
            animator.SetBool("Right", true);
            transform.localScale = new Vector3(-scale2change, transform.localScale.y, 1);
            GameManager.Instance.flipWeapon = true;

        }
        else
        {
            animator.SetBool("Right", false);
            gameObject.GetComponent<SpriteRenderer>().flipX = false;
            transform.localScale = new Vector3(scale2change, transform.localScale.y, 1);
            GameManager.Instance.flipWeapon = false;
        }
        if (angle >= 45 && angle < 135)
        {
            animator.SetBool("Up", true);
        }
        else
        {
            animator.SetBool("Up", false);
        }
        if (angle >= -135 && angle < -45)
        {
            animator.SetBool("Down", true);
        }
        else
        {
            animator.SetBool("Down", false);
        }
        if (angle >= 135 || angle < -135)
        {
            animator.SetBool("Left", true);
        }
        else
        {
            animator.SetBool("Left", false);
        }
    }
}

