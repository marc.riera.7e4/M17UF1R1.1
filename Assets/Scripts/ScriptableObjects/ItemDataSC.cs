﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ItemDataSC : ScriptableObject
{
    public Sprite Icon;
}
