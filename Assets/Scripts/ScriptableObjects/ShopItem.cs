﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Stat { Hp, Speed, Damage, Ammo, FireRate }
public enum Char { Player, Enemy }

[CreateAssetMenu(fileName = "NewShopItem", menuName = "ScriptableObjects/ShopItem", order = 1)]
public class ShopItem : ScriptableObject
{
    public string itemName;
    public Stat stat;
    public Char character;
    public float mult;
    public int price;
}
