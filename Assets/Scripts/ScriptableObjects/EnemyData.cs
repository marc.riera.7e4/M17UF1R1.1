﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Enemy", menuName = "ScriptableObjects/CharacterData/Enemy", order = 1)]
public class EnemyData : CharacterData
{
    public float dmg;
    public string description;
}
