﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Consumible", menuName = "ScriptableObjects/Active/Consumables", order = 1)]
public class Consumible : Items
{
    public float heal;
    public float mana;
    public float speed;
    public float duration;
}