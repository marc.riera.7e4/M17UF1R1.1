﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


    [CreateAssetMenu(fileName = "Buff", menuName = "ScriptableObjects/Active/Buffs", order = 1)]
    public class Buffs : Items
    {
        public float Damage;
        public float Speed;
        public float FireRate;
        public float MunicionMax;
        
    }


