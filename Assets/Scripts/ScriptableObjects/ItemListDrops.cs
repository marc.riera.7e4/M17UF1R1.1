﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ItemListDrops", menuName = "ScriptableObjects/ItemListDrops", order = 1)]
public class ItemListDrops : ScriptableObject
{
    public List<ItemDataSC> items;

    public GameObject ItemDropablePrefab;

    public GameObject getRandomItem()
    {
        //Random funtionality
        GameObject item = Instantiate(ItemDropablePrefab);
        ItemDataSC itemData = items[Random.Range(0, items.Count)];

        item.GetComponent<SpriteRenderer>().sprite = itemData.Icon;
        //item.GetComponent<ItemController>().ItemData = (WeaponSc)itemData;
        return item;
    }
}
