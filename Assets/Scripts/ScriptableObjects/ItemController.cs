﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemController : MonoBehaviour
{
    public WeaponSc ItemData;

    public bool shoot = false;
    public GameObject bullet;
    void Update()
    {
        //gameObject.GetComponent<SpriteRenderer>().sprite = ItemData.Icon;

        if (GameManager.Instance.move == true)
        {
            gameObject.GetComponent<SpriteRenderer>().enabled = false;

        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().enabled = true;
        }

        if (Input.GetMouseButtonDown(0) && shoot == true && GameManager.Instance.move == false)
        {
            GameObject bulletClone = Instantiate(bullet);
            bulletClone.transform.position = this.transform.position;
            //bulletClone.transform.rotation = Quaternion.Euler(0, 0, angle);

            bulletClone.GetComponent<Rigidbody2D>().velocity = transform.right * 10;
            shoot = false;
            StartCoroutine(cd());
        }
    }
    IEnumerator cd()
    {
        yield return new WaitForSeconds(1);
        shoot = true;
    }
}
