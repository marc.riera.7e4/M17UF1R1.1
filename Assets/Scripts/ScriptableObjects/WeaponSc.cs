﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum extraEffect { none, freeze }

[CreateAssetMenu(fileName = "WeaponData", menuName = "ScriptableObjects/Active/WeaponSc", order = 3)]

public class WeaponSc : Items
{
    public float dmg;
    public float ammo;
    public float maxBulletCapacity;
    public float fireRate;
    public int bulletXShoot;
    public float range;
    public float speed;
    public extraEffect extraEffect;
    public AudioClip sound;
}
