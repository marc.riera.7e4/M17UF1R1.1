﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Items : ScriptableObject
{
    public int ID;
    public Sprite icon;
    public rarity chance;
    public int price;

    public enum rarity
    {
        Common,
        Uncommon,
        Rare,
        Legendary
    }
}
