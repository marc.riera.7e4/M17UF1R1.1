﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Player", menuName = "ScriptableObjects/CharacterData/Player", order = 1)]
public class PlayerData : CharacterData
{
    public int exp;
    public string description;
    public Inventory inv;


    public class Inventory
    {

    }
}
