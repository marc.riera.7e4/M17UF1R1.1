﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dropItemController : MonoBehaviour
{
    [SerializeField]
    private ItemListDrops itemsList;
    private List<ItemDataSC> itemList;

    
    // Start is called before the first frame update
    void Start()
    {
        Invoke("getRandomItem4Invoke", 1);

    }

    // Update is called once per frame
    void Update()
    {
         
    }
    void getRandomItem4Invoke()
    {
        GameObject item = itemsList.getRandomItem();
        item.transform.position = this.gameObject.transform.position;
    }
}
