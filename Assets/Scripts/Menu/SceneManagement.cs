﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagement : MonoBehaviour
{

    private Scenes _currentScene;

    public enum Scenes
    {
        Menu, Shop, Achievements, Game, Score, Options
    }

    public Scenes CurrentScene
    {
        get => _currentScene;
        set
        {
            _currentScene = value;
            ChangeScene();
        }
    }

    private void Start()
    {
        Scene scene = SceneManager.GetActiveScene();
        string activeScene;
        activeScene = scene.name;
        _currentScene = (Scenes)System.Enum.Parse(typeof(Scenes), activeScene);
    }

    void ChangeScene()
    {
        Debug.Log("SCENE CHANGED");
        switch (_currentScene)
        {
            case Scenes.Menu:
                GoToMenu();
                break;
            case Scenes.Shop:
                GoToShop();
                break;
            case Scenes.Achievements:
                GoToAchievements();
                break;
            case Scenes.Game:
                GoToGame();
                break;
            case Scenes.Score:
                GoToScore();
                break;
            case Scenes.Options:
                GoToOptions();
                break;
        }
    }

    void GoToMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    void GoToShop()
    {
        SceneManager.LoadScene("Shop");
    }

    void GoToAchievements()
    {
        SceneManager.LoadScene("Achievements");
    }

    void GoToGame()
    {
        SceneManager.LoadScene("Game");
    }

    void GoToScore()
    {
        SceneManager.LoadScene("Score");
    }
    void GoToOptions()
    {
        SceneManager.LoadScene("Options");
    }
}
