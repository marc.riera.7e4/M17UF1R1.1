﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneRelatedUI : MonoBehaviour
{
    SceneManagement scene = new SceneManagement();
    private void Update()
    {
       
    }
    public void LoadMenu()
    {
        scene.CurrentScene = SceneManagement.Scenes.Menu;
    }

    public void LoadShop()
    {
        scene.CurrentScene = SceneManagement.Scenes.Shop;
    }

    public void LoadGame()
    {
        
        scene.CurrentScene = SceneManagement.Scenes.Game;
    }

    public void LoadScore()
    {
        scene.CurrentScene = SceneManagement.Scenes.Score;
    }

    public void LoadOptions()
    {
        Debug.Log(scene.CurrentScene);
        GameManager.Instance.lastScene = SceneManager.GetActiveScene().name;
        scene.CurrentScene = SceneManagement.Scenes.Options;
    }
    public void loadBack()
    {
        if(GameManager.Instance.lastScene == "Menu")
        {
            scene.CurrentScene = SceneManagement.Scenes.Menu;
        } else if (GameManager.Instance.lastScene == "Score")
        {
            scene.CurrentScene = SceneManagement.Scenes.Score;
        } else if (GameManager.Instance.lastScene == "Shop")
        {
            scene.CurrentScene = SceneManagement.Scenes.Shop;
        }
    }
}
