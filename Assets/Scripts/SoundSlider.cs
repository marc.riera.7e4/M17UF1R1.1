﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundSlider : MonoBehaviour
{
   public AudioMixer mixer;
    public string audio;
    public void setlevel(float sliderValue)
    {
        mixer.SetFloat(audio, Mathf.Log10(sliderValue) * 20);
    }
}
